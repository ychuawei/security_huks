# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//base/security/huks/build/config.gni")
import("//base/security/huks/huks.gni")
import("//build/ohos.gni")
import("//build/test.gni")

module_output_path = "huks_standard/huks_UT_test"

ohos_unittest("huks_UT_test") {
  module_out_path = module_output_path

  sources = [
    "src/hks_aes_cipher_part1_test.cpp",
    "src/hks_aes_cipher_part2_test.cpp",
    "src/hks_aes_cipher_part3_test.cpp",
    "src/hks_aes_cipher_test_common.cpp",
    "src/hks_attest_key_nonids_test.cpp",
    "src/hks_attest_key_test_common.cpp",
    "src/hks_check_pur_part_test.cpp",
    "src/hks_cross_test.cpp",
    "src/hks_cross_test_common.cpp",
    "src/hks_dh_agree_test.cpp",
    "src/hks_ecc_sign_verify_part1_test.cpp",
    "src/hks_ecc_sign_verify_part2_test.cpp",
    "src/hks_ecc_sign_verify_part3_test.cpp",
    "src/hks_ecc_sign_verify_test_common.cpp",
    "src/hks_ecdh_agree_part1_test.cpp",
    "src/hks_ecdh_agree_part2_test.cpp",
    "src/hks_ecdh_agree_test_common.cpp",
    "src/hks_ed25519_sign_verify_test.cpp",
    "src/hks_export_test.cpp",
    "src/hks_export_test_mt.cpp",
    "src/hks_hkdf_derive_part1_test.cpp",
    "src/hks_hkdf_derive_part2_test.cpp",
    "src/hks_hkdf_derive_test_common.cpp",
    "src/hks_hmac_test.cpp",
    "src/hks_import_wrapped_ecdh_suite_test.cpp",
    "src/hks_import_wrapped_sm2_suite_test.cpp",
    "src/hks_import_wrapped_test_common.cpp",
    "src/hks_import_wrapped_x25519_suite_test.cpp",
    "src/hks_pbkdf2_derive_part1_test.cpp",
    "src/hks_pbkdf2_derive_part2_test.cpp",
    "src/hks_pbkdf2_derive_test_common.cpp",
    "src/hks_rsa_cipher_part1_test.cpp",
    "src/hks_rsa_cipher_part2_test.cpp",
    "src/hks_rsa_cipher_part3_test.cpp",
    "src/hks_rsa_cipher_part4_test.cpp",
    "src/hks_rsa_cipher_part5_test.cpp",
    "src/hks_rsa_cipher_test_common.cpp",
    "src/hks_rsa_sign_verify_part1_test.cpp",
    "src/hks_rsa_sign_verify_part2_test.cpp",
    "src/hks_rsa_sign_verify_part3_test.cpp",
    "src/hks_rsa_sign_verify_part4_test.cpp",
    "src/hks_rsa_sign_verify_part5_test.cpp",
    "src/hks_rsa_sign_verify_part6_test.cpp",
    "src/hks_rsa_sign_verify_part7_test.cpp",
    "src/hks_rsa_sign_verify_part8_test.cpp",
    "src/hks_rsa_sign_verify_part9_test.cpp",
    "src/hks_rsa_sign_verify_test_common.cpp",
    "src/hks_three_stage_test_common.c",
    "src/hks_x25519_agree_test.cpp",
  ]

  sources += [
    "src/hks_import_agree_test.cpp",
    "src/hks_import_key_test.cpp",
    "src/hks_import_rsa_test.cpp",
    "src/hks_import_sign_verify_test.cpp",
  ]

  sources += [
    "src/hks_chipset_platform_decrypt_test.cpp",
    "src/hks_chipset_platform_encrypt_test.cpp",
  ]

  defines = [
    "L2_STANDARD",
    "_HUKS_LOG_ENABLE_",
  ]

  if (use_crypto_lib == "openssl") {
    defines += [ "_USE_OPENSSL_" ]
    sources += [
      "src/hks_dsa_sign_verify_test.cpp",
      "src/hks_sm2_sign_verify_test.cpp",
      "src/hks_sm4_cipher_part_test.cpp",
      "src/hks_sm4_cipher_test_common.cpp",
    ]
  }
  if (use_crypto_lib == "mbedtls") {
    defines += [ "_USE_MBEDTLS_" ]
  }
  if (enable_mock) {
    sources += [
      "src/hks_access_control_agree_test.cpp",
      "src/hks_access_control_cipher_test.cpp",
      "src/hks_access_control_derive_test.cpp",
      "src/hks_access_control_mac_test.cpp",
      "src/hks_access_control_rsa_sign_verify_test.cpp",
      "src/hks_access_control_rsa_sign_verify_test_common.cpp",
      "src/hks_access_control_secure_sign_test.cpp",
      "src/hks_access_control_test_common.cpp",
      "src/hks_check_auth_part_test.cpp",
    ]
  }

  include_dirs = [
    "//base/security/huks/test/unittest/huks_standard/three_stage_test/include",
    "//commonlibrary/c_utils/base/include",
    "include",
    "//test/xts/acts/security_lite/huks/common/include",
    "//base/security/huks/interfaces/inner_api/huks_standard/main/include",
    "//base/security/huks/frameworks/huks_standard/main/common/include/",
    "//base/security/huks/test/unittest/src/common/include",
    "//base/security/huks/utils/crypto_adapter",
    "//base/security/huks/services/huks_standard/huks_service/main/core/include",
  ]
  configs = [ "//base/security/huks/frameworks/config/build:coverage_flag" ]
  deps = [
    "//base/security/access_token/interfaces/innerkits/nativetoken:libnativetoken",
    "//base/security/access_token/interfaces/innerkits/token_setproc:libtoken_setproc",
    "//base/security/huks/frameworks/huks_standard/main:huks_standard_frameworks",
    "//base/security/huks/interfaces/inner_api/huks_standard/main:libhukssdk",
    "//base/security/huks/services/huks_standard/huks_engine/main/core:huks_engine_core_standard",
    "//base/security/huks/services/huks_standard/huks_engine/main/core_dependency:libhuks_core_hal_api_static",
  ]
  deps += [
    "//base/security/huks/services/huks_standard/huks_service/main/core:libhuks_service_core_standard_static",
    "//base/security/huks/services/huks_standard/huks_service/main/os_dependency/idl:libhuks_service_idl_standard_static",
    "//base/security/huks/utils/crypto_adapter:libhuks_utils_client_service_adapter_static",
  ]
  deps += [
    # ld.lld: error: undefined symbol: HksRwlockClose
    "//base/security/huks/services/huks_standard/huks_service/main/os_dependency:libhuks_service_os_dependency_standard_static",

    # used by hks_chipset_platform_encrypt_test.cpp
    "//third_party/json:nlohmann_json_static",
  ]
  cflags = [ "-DHKS_CONFIG_KEY_STORE_PATH=\"${huks_key_store_standard_path}\"" ]
  external_deps = [ "hiviewdfx_hilog_native:libhilog" ]
}
